//
// Created by sylee on 2019-12-05.
//

#include <stdio.h>

int subtract(){
    int a, b;
    int value;
    printf("a의 값 : ");
    scanf("%d", &a);
    printf("b의 값 : ");
    scanf("%d", &b);
    do{
        puts("a보다 큰 값을 입력하세요!");
        printf("b의 값 : ");
        scanf("%d", &b);
    }while(a > b);

    printf("b-a는 %d입니다.", b-a);

    return 0;
}