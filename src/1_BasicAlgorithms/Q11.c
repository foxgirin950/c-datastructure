//
// Created by sylee on 2019-12-16.
//

// Q11 양의 정수의 자릿수 구하기
#include <stdio.h>

int main(void){

    int n;
    int digits = 0; //자릿수

    do{
        printf("양의 정수 : ");
        scanf("%d", &n);
    }while(n <= 0);

    while(n > 0){
        n /= 10;
        digits++;
    }

    printf("그 수는 %d자리 입니다.", digits);

    return 0;
}